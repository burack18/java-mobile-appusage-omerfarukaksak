package be.ucll.java.mobile.appusage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Provide views to RecyclerView with the directory entries.
 */
public class MyAdapter extends RecyclerView.Adapter<MyViewHolder> {

    private List<MyUsageStats> myUsageStats = new ArrayList<>(25);
    private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", new Locale("nl", "BE"));

    public MyAdapter() {
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.usage_row, viewGroup, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder viewHolder, final int position) {
        viewHolder.getPackageName().setText(myUsageStats.get(position).usageStats.getPackageName());
        long lastTimeUsed = myUsageStats.get(position).usageStats.getLastTimeUsed();
        viewHolder.getLastTimeUsed().setText(" " + dateFormat.format(new Date(lastTimeUsed)));
        viewHolder.getAppIcon().setImageDrawable(myUsageStats.get(position).appIcon);
    }

    @Override
    public int getItemCount() {
        return myUsageStats.size();
    }

    public void setCustomUsageStatsList(List<MyUsageStats> myUsageStats) {
        this.myUsageStats = myUsageStats;
    }
}