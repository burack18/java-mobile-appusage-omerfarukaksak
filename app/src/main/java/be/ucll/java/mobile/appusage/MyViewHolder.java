package be.ucll.java.mobile.appusage;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Provide a reference to the type of views that you are using (custom ViewHolder)
 */
public class MyViewHolder extends RecyclerView.ViewHolder {
    private final TextView txtPackageName;
    private final TextView txtLastTimeUsed;
    private final ImageView icoApp;

    public MyViewHolder(View v) {
        super(v);
        txtPackageName = v.findViewById(R.id.txtPackageName);
        txtLastTimeUsed = v.findViewById(R.id.txtLastTimeUsed);
        icoApp = v.findViewById(R.id.icoApp);
    }

    public TextView getLastTimeUsed() {
        return txtLastTimeUsed;
    }

    public TextView getPackageName() {
        return txtPackageName;
    }

    public ImageView getAppIcon() {
        return icoApp;
    }
}
